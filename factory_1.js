console.log.apply( console, ['It', 'is', 'running on', jQuery.fn.jquery] );
/* ****
 * Factory object construction in lieu of new operator
 * var Mammal = Mammal("cat");
 * var Dog = Dog("His name is: Hunter");
 * ****/
 /* Baseline Constructor */
 function Mammal( name ) {
 	return {
 		constructor : Mammal,
	 	init : (function() {
	 		console.log( 'A ' + name + ' has been initialized.' );
	 	}()),
	 	printBreed : function() {
	 		console.log.apply( console, ['Animal breed = ', name] );
	 	}
 	};
 };
 Mammal.properties = {
 	hasEyes : true,
 	hasHair : true
 };
 Mammal.instincts = {
 	dietType : null,
 	bites : null,
 	licks : null,
 	setDiet : function( diet ) {
 		this.dietType = diet;
 	},
 	setFur : function( fur ) {
 		this.hasFur = fur;
 	},
 	itBites : function( bite ) {
 		this.bites = bite;
 	},
 	itLicks : function( lick ) {
 		this.licks = lick;
 	}
 };

 function Cat( name ) {
 	var cat = Mammal( name );
 	cat.meow = function() {
 		this.printBreed();
 		console.log( 'Meooowww!' );
 	};
 	return cat;
 };